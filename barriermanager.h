
// XineramaBarriers
// ----------
// Copyright (C) 2011, Timur Kristóf
//
// This X client application will create pointer barriers
// based on the Xinerama information of your current driver.
// Only works with XFixes v5 or higher and the Xorg server
// which has the pointer barriers patch.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef BARRIERMANAGER_H
#define BARRIERMANAGER_H

#include <X11/extensions/Xinerama.h>
#include <X11/extensions/Xfixes.h>
#include <X11/extensions/xfixesproto.h>
#include <vector>

#define MIN(a, b) (((a) < (b)) ? a : b)

class BarrierManager {
    std::vector<PointerBarrier> barriers;
    std::vector<XineramaScreenInfo> infos;
    XWindowAttributes attrs;
    Display *d;

public:
    explicit BarrierManager(Display *d);
    ~BarrierManager();
    void updateBarriers();
    void destroyBarriers();
};

#endif // BARRIERMANAGER_H
