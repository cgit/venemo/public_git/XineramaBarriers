
// XineramaBarriers
// ----------
// Copyright (C) 2011, Timur Kristóf
//
// This X client application will create pointer barriers
// based on the Xinerama information of your current driver.
// Only works with XFixes v5 or higher and the Xorg server
// which has the pointer barriers patch.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "barriermanager.h"
#include <iostream>

int main()
{
    std::cout << "good morning!" << std::endl;
    Display *d = XOpenDisplay(0);

    if (!XineramaIsActive(d))
    {
        std::cout << "Xinerama is not active, bye!" << std::endl;
        return -1;
    }

    BarrierManager m(d);
    XSelectInput(d, DefaultRootWindow(d), StructureNotifyMask);
    XEvent ev;

    while (XNextEvent(d, &ev))
    {
        switch (ev.type)
        {
        case ConfigureNotify:
            std::cout << "configure event" << std::endl;
            m.updateBarriers();
            break;
        }
    }

    XCloseDisplay(d);
    return 0;
}
