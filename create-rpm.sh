PROJECT='XineramaBarriers'
VERSION='1.0'

TARBALLDIR=$PROJECT-$VERSION

rm $TARBALLDIR.tar.gz
mkdir $TARBALLDIR
cp * $TARBALLDIR/
tar -cf $TARBALLDIR.tar $TARBALLDIR
rm -rf $TARBALLDIR
gzip $TARBALLDIR.tar

cp $TARBALLDIR.tar.gz ~/rpmbuild/SOURCES/
cp $PROJECT.spec ~/rpmbuild/SPECS/
rpmbuild -ba ~/rpmbuild/SPECS/$PROJECT.spec
