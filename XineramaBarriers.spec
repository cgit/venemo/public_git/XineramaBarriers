Name:           XineramaBarriers
Version:        1.0
Release:        1%{?dist}
Summary:        Creates X pointer barriers from Xinerama information

Group:          Applications/System
License:        GPLv2+
URL:            git://fedorapeople.org/~venemo/XineramaBarriers.git
Source0:        http://sources.venemo.net/%{name}-%{version}.tar.gz

# this is for qmake, Qt itself is not required by the package
BuildRequires: pkgconfig(QtCore)
BuildRequires: pkgconfig(x11), pkgconfig(xinerama), pkgconfig(xfixes)

%description 
XineramaBarriers is an X client application which
will create pointer barriers
based on the Xinerama information of your current driver.
Only works with XFixes v5 or higher and the Xorg server
which has the pointer barriers patch.

%prep
%setup -q

%build
# This will find qmake on both Fedora and MeeGo
qmake-qt4 || qmake
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make INSTALL_ROOT=$RPM_BUILD_ROOT install

%files
# Setting default file attributes
%defattr(-,root,root,-)
# Listing the files in the package
%{_bindir}/XineramaBarriers
# Documentation
%doc LICENSE

%changelog
* Sun Apr 28 2011 Timur Kristóf <venemo@msn.com> 1.0-1
- Initial version of the package in RPM
